var data = [
	[
		{
			title: 'Тайная жизнь домашних животных',
			video: 'https://www.youtube.com/embed/aTcvtZd9ZV0',
			class: 'one',
			age: '6+',
			seanse: [
				[
					{
						time: '20:50',
						price: '400',
						format: []
					},
					{
						time: '22:50',
						price: '400',
						format: []
					},
					{
						time: '23:50',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '00:50',
						price: '400',
						format: []
					}
				]
			]
		},
		{
			title: 'Парни со стволами',
			video: 'https://www.youtube.com/embed/GGnwNi2lzak',
			class: 'one',
			age: '18+',
			seanse: [
				[
					{
						time: '20:50',
						price: '400',
						format: []
					},
					{
						time: '22:50',
						price: '400',
						format: []
					},
					{
						time: '23:50',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '00:50',
						price: '400',
						format: []
					}
				]
			]
		}
	],
	[
		{
			title: 'Пит и его дракон',
			video: 'https://www.youtube.com/embed/gfxsYpTNpTs',
			class: 'one',
			age: '6+',
			seanse: [
				[
					{
						time: '20:50',
						price: '400',
						format: []
					},
					{
						time: '22:50',
						price: '400',
						format: []
					},
					{
						time: '23:50',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '00:50',
						price: '400',
						format: []
					}
				]
			]
		},
		{
			title: 'Афера под прикрытием',
			video: 'https://www.youtube.com/embed/TLW16vW4mm8',
			class: 'two',
			age: '18+',
			seanse: [
				[
					{
						time: '10:20',
						price: '300',
						format: []
					},
					{
						time: '11:30',
						price: '400',
						format: []
					},
					{
						time: '12:40',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '16:50',
						price: '400',
						format: []
					},
					{
						time: '17:50',
						price: '400',
						format: []
					},
					{
						time: '19:50',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '20:50',
						price: '400',
						format: []
					},
					{
						time: '22:50',
						price: '400',
						format: []
					},
					{
						time: '23:50',
						price: '400',
						format: []
					}
				],
				[
					{
						time: '00:50',
						price: '400',
						format: []
					}
				]
			]
		}
	]
];

var infoplasma = document.getElementById('infoplasma');
var table = document.createElement('table');
var swiper = [];
var swiperSlide = createElem('div', 'swiper-slide');
var containerarray = ['one', 'two', 'three', 'four', 'five','six', 'seven'];

function createElem(elem, className) {
	var element = document.createElement(elem);
	if (className) {
		element.className = className;
	}
	return element;
}
function createTrailer(video) {

	var iframe = createElem('iframe');
	iframe.src=video + "?autoplay=0&controls=0&fs=0&loop=1&rel=0&showinfo=0&volume=0";
	iframe.frameborder=0;
	var videoWrapper = createElem('div','video-wrapper');
	videoWrapper.appendChild(iframe);
	var td =  createElem('td', 'video');
	td.appendChild(videoWrapper);
	return td;
}
function createTitle(title) {
	var tdTitle = createElem('td', 'title');
	tdTitle.innerHTML = title;
	return tdTitle;
}
function createShedule() {
	var tdSeanse = createElem('td', 'schedule');
	return tdSeanse;
}
function createSlider(extraclass) {
	var swiperContainer =  createElem('div', "swiper-container "+extraclass);

	return swiperContainer;
}
function createSlide() {
	var swiperSlide = createElem('div', 'swiper-slide');
	return swiperSlide;
}
function createSeance(seanseData) {
	var seanseItem = createElem('div', 'seanse-item');
	swiperSlide.appendChild(seanseItem);
	var timeDiv = createElem('div', 'time');
	seanseItem.appendChild(timeDiv);
	timeDiv.innerHTML = seanseData.time;
	var bottomDiv = createElem('div', 'bottom');
	seanseItem.appendChild(bottomDiv);
	var priceDiv = createElem('div', 'price ruble');
	bottomDiv.appendChild(priceDiv);
	priceDiv.innerHTML = seanseData.price;
	var formatDiv = createElem('div', 'format');
	var DataFormat = seanseData.format.join(' ');
	formatDiv.setAttribute('data-format', DataFormat)
	bottomDiv.appendChild(formatDiv);
	return seanseItem;
}


for (var tableCount = 0; tableCount < data.length; tableCount++ ) {
	//console.log('create table ' + tableCount);
	var table = createElem('table');
	var trTitle = createElem('tr');
	var trVideo = createElem('tr');
	var trSeanse = createElem('tr');
 	for (var filmCount = 0; filmCount < data[tableCount].length; filmCount++ ) {
		//console.log('create film ' +  filmCount);
	    var createrTitle = createTitle(data[tableCount][filmCount].title)
	    trTitle.appendChild(createrTitle);

	    var ageLimit = createElem('div', 'age-limit');
	    ageLimit.innerHTML=data[tableCount][filmCount].age;
	    createrTitle.appendChild(ageLimit);

	    trVideo.appendChild(createTrailer(data[tableCount][filmCount].video));


	    var createSchedule = createShedule();

	    var createSliders = createSlider(containerarray[filmCount]);
	    var swiperWrapper = createElem('div', 'swiper-wrapper');


	    for (var slideCount = 0; slideCount < data[tableCount][filmCount].seanse.length; slideCount++ ) {
	    	var createSlides = createSlide();
		    var seanse = createElem('div', 'seanse');
		    for (var seanseCount = 0; seanseCount < data[tableCount][filmCount].seanse[slideCount].length; seanseCount++ ) {
			    //console.log('seance ' + seanseCount + ' : ' + data[tableCount][filmCount].seanse[slideCount][seanseCount].time);
				var createsSeanse = createSeance(data[tableCount][filmCount].seanse[slideCount][seanseCount]);
			    seanse.appendChild(createsSeanse);
		    }
		    swiperWrapper.appendChild(createSlides);
		    createSlides.appendChild(seanse);
	    }

	    createSliders.appendChild(swiperWrapper);
	    createSchedule.appendChild(createSliders);

	    trSeanse.appendChild(createSchedule);
	}
	table.appendChild(trTitle);
	table.appendChild(trVideo);
	table.appendChild(trSeanse);
	infoplasma.appendChild(table);
}