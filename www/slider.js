var swiper = new Swiper('.schedule .swiper-container.one', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperTwo = new Swiper('.schedule .swiper-container.two', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperThree = new Swiper('.schedule .swiper-container.three', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperFour = new Swiper('.schedule .swiper-container.four', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperFive = new Swiper('.schedule .swiper-container.five', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperSix = new Swiper('.schedule .swiper-container.six', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});
var swiperSeven = new Swiper('.schedule .swiper-container.seven', {
	effect: 'cube',
	grabCursor: true,
	direction: 'vertical',
	autoplay: 4000,
	speed: 600,
	loop: true,
	cube: {
		shadow: false,
		slideShadows: false,
		shadowOffset: 20,
		shadowScale: 0.94
	}
});