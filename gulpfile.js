var gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefixer = require('gulp-autoprefixer'),
	watch = require('gulp-watch'),
	cssmin = require('gulp-minify-css'),
	/*pug = require('gulp-pug'),*/
	browserSync = require('browser-sync').create();


// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

	browserSync.init({
		server: "./www"
	});

	gulp.watch("./scss/*.*", ['sass']);
	/*gulp.watch("./views/!*.*", ['views']);*/
});
/*PUG файлы
gulp.task('views', function buildHTML() {
	return gulp.src('views/!*.pug')
		.pipe(pug({}))
		.pipe(gulp.dest("./www/"))
		.pipe(browserSync.stream());
});*/

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
	return gulp.src("./scss/*.scss")
		.pipe(sass({errLogToConsole: true}))
		.pipe(prefixer({ browsers: ['last 12 versions'] }))
		.pipe(cssmin())
		.pipe(gulp.dest("./www/"))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve']);